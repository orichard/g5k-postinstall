build:
	mkdir -p build && \
	tar -czf build/g5k-postinstall.tgz README.md g5k-postinstall lib ssh_host_keys authorized_keys version

build-and-push: build
	rsync -avzP build/g5k-postinstall.tgz nancy.g5k:public/postinstalls/

.PHONY: build build-and-push
