# Rakefile
# Author:: Pascal Morillon (<pascal.morillon@irisa.fr>)
# Author:: Sebastien Badia (<sebastien.badia@inria.fr>)
# Date:: Thu Feb 15 16:26:36 +0100 2011
#

require 'rake'
require 'yaml'

SITE = ENV['SITE']
CONF = YAML::load(File.open('grid5000.yml'))

verbose(true)

desc "Setup the SSH host keys for the cluster nodes (to be done only once)"
task :setup do
  raise "MUST provide a SITE=<sitename>" unless ENV['SITE']
  init
  @clusters.each do|cluster,carac|
    size=carac[0]
    puts "--> configuring cluster #{cluster} (featuring #{size} nodes)"
    for i in (1..size)
      hostname = "#{cluster}-#{i}"
      ip = "#{carac[1]}.#{i}"
      puts "    #{hostname} (#{ip})"
      sshdir  = "ssh_host_keys/#{SITE}/#{cluster}/#{ip}"
      puts "    => creating directory #{sshdir}"
      sh "mkdir -p #{sshdir}"
      puts "    => creating SSH host key (rsa) for #{hostname}"
      sh "ssh-keygen -t rsa -C \"#{hostname}.#{SITE}.grid5000.fr\" -P \"\" -f #{sshdir}/ssh_host_rsa_key"
      puts "    => creating SSH host key (ecdsa) for #{hostname}"
      sh "ssh-keygen -t ecdsa -C \"#{hostname}.#{SITE}.grid5000.fr\" -P \"\" -f #{sshdir}/ssh_host_ecdsa_key"
      puts "    => creating SSH host key (ed25519) for #{hostname}"
      sh "ssh-keygen -t ed25519 -C \"#{hostname}.#{SITE}.grid5000.fr\" -P \"\" -f #{sshdir}/ssh_host_ed25519_key"
    end
  end
end

desc "Clean the SSH host keys for the cluster nodes"
task :clean do
  raise "MUST provide a SITE=<sitename>" unless ENV['SITE']
  init
  @clusters.each do|cluster,carac|
    puts "--> deleting cluster #{cluster}"
    dir="ssh_host_keys/#{SITE}/#{cluster}"
    puts "=> deleting #{dir}/"
    sh "rm -rf #{dir}" if File.exist?("#{dir}")
  end
end

def init
  raise "MUST provide a CLUSTER=<clustername | all>" unless ENV['CLUSTER']
  if ENV['CLUSTER'] != "all"
    @clusters = { ENV['CLUSTER'] => CONF[SITE][ENV['CLUSTER']] }
    puts @clusters.to_yaml
  else
    @clusters = CONF[SITE]
  end
end
