def beegfs_configure_mount(server, host, client_port, helper_port, mgmtd_port)
  FileUtils::mkdir_p("#{DSTDIR}/etc/beegfs/#{server}.d")

  File::open("#{DSTDIR}/etc/beegfs/#{server}.d/beegfs-helperd.conf", "w") do |fd|
    fd.puts <<-EOF
connAuthFile       =
connHelperdPortTCP = #{helper_port}
connPortShift      = 0

logNoDate          = false
logNumLines        = 50000
logNumRotatedFiles = 5
logStdFile         = /var/log/beegfs-client.log

runDaemonized      = true

tuneNumWorkers     = 2
    EOF
  end

  File::open("#{DSTDIR}/etc/beegfs/#{server}.d/beegfs-client.conf", "w") do |fd|
    fd.puts <<-EOF
sysMgmtdHost                  = #{host}

connAuthFile                  =
connClientPortUDP             = #{client_port}
connHelperdPortTCP            = #{helper_port}
connMgmtdPortTCP              = #{mgmtd_port}
connMgmtdPortUDP              = #{mgmtd_port}
connPortShift                 = 0

connCommRetrySecs             = 600
connFallbackExpirationSecs    = 900
connInterfacesFile            =
connMaxInternodeNum           = 12
connNetFilterFile             =

connUseRDMA                   = true
connRDMABufNum                = 70
connRDMABufSize               = 8192
connRDMATypeOfService         = 0
connTcpOnlyFilterFile         =

logClientID                   = false
logHelperdIP                  =
logLevel                      = 3
logType                       = helperd

quotaEnabled                  = false

sysCreateHardlinksAsSymlinks  = false
sysMountSanityCheckMS         = 11000
sysSessionCheckOnClose        = false
sysSyncOnClose                = false
sysTargetOfflineTimeoutSecs   = 900
sysUpdateTargetStatesSecs     = 60
sysXAttrsEnabled              = false

tuneFileCacheType             = buffered
tunePreferredMetaFile         =
tunePreferredStorageFile      =
tuneRemoteFSync               = true
tuneUseGlobalAppendLocks      = false
tuneUseGlobalFileLocks        = false

sysACLsEnabled                = false
    EOF
  end

end

def beegfs_configure
  File::open("#{DSTDIR}/etc/default/beegfs-helperd", "w") do |fd|
    fd.puts <<-EOF
START_SERVICE="YES"
MULTI_MODE="YES"
    EOF
  end
  File::open("#{DSTDIR}/etc/beegfs/beegfs-mounts.conf", "w") do |fd|
    fd.puts <<-EOF
/grcinq /etc/beegfs/grcinq.d/beegfs-client.conf
/grvingt /etc/beegfs/grvingt.d/beegfs-client.conf
    EOF
  end

  beegfs_configure_mount("grcinq", "grcinq-srv-1", 8004, 8006, 8008)
  beegfs_configure_mount("grvingt", "grvingt-srv", 8104, 8106, 8108)

  FileUtils::mkdir_p("#{DSTDIR}/grcinq")
  FileUtils::mkdir_p("#{DSTDIR}/grvingt")

  FileUtils::ln_s("/lib/systemd/system/beegfs-helperd@.service", "#{DSTDIR}/etc/systemd/system/multi-user.target.wants/beegfs-helperd@grcinq.service")
  FileUtils::ln_s("/lib/systemd/system/beegfs-helperd@.service", "#{DSTDIR}/etc/systemd/system/multi-user.target.wants/beegfs-helperd@grvingt.service")
  FileUtils::ln_s("/lib/systemd/system/beegfs-client.service", "#{DSTDIR}/etc/systemd/system/multi-user.target.wants/beegfs-client.service")
end
