require 'logger'
require 'optparse'
require 'erb'
require 'json'
require 'fileutils'
require 'yaml'

def system_or_raise(cmd)
  system(cmd) or raise("Command failed: #{cmd}")
end

require 'g5k-postinstall/config'
require 'g5k-postinstall/multiio'
require 'g5k-postinstall/node-info'
require 'g5k-postinstall/node-status'
require 'g5k-postinstall/hacks'
require 'g5k-postinstall/bootloader'
require 'g5k-postinstall/os-detection'
