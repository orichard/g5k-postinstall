require 'ipaddr'
require 'open-uri'
require 'openssl'

# Parse IP address from ip -o addr output
def get_net_cfg
  nic_with_ip = `ip -o addr`.split("\n").select { |l| l !~ /: lo/ and l =~ / inet /}.first
  ip = nic_with_ip.scan(/inet ([0-9.]+)/).first.first
  nic = nic_with_ip.split(/\s+/)[1]
  mac = `ip -o link show dev #{nic}`.scan(/ether ([0-9a-f:]+)/).first.first
  return nic, ip, mac
end

def get_site(ip)
  SITE_IP_RANGE.each do |s|
    return s[0] if IPAddr::new(s[1]).include?(ip)
  end
end

# Try to fetch the JSON description of the cluster
# If it fails, fall back to using a local cache
def get_json(site, cluster, ip)
  tries = 3
  d = nil
  begin
    url = "https://api.grid5000.fr/3.0/sites/#{site}/clusters/#{cluster}/nodes.json"
    $logger.info("Fetching #{url}")
    d = open(url, {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}).read
  rescue
    tries -= 1
    if tries > 0
      $logger.info("Fetching #{url} failed. Sleeping 1s and retrying.")
      sleep(1)
      retry
    end
  end

  if d.nil?
    tries = 3
    begin
      url = "https://api.grid5000.fr/3.0/sites/#{site}/clusters/#{cluster}/nodes.json?branch=testing"
      $logger.info("Fetching #{url}")
      d = open(url, {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}).read
    rescue
      tries -= 1
      if tries > 0
        $logger.info("Fetching #{url} failed. Sleeping 1s and retrying.")
        sleep(1)
        retry
      end
    end
  end

  if d.nil?
    STDERR.puts("Fetching #{url} failed too many times. Please report to support-staff@lists.grid5000.fr.")
    exit(1)
  end
  j = JSON::parse(d)['items']
  return j.select { |n| not n['network_adapters'].select { |na| na['ip'] == ip }.empty? }.first
end
