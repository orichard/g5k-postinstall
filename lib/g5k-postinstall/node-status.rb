# Try to fetch the JSON status of the node and parse it to extract the user who
# reserved the node, only for a deploy job.
def get_node_current_user(site, cluster, node)
  tries = 3
  begin
    url = "https://api.grid5000.fr/3.0/sites/#{site}/clusters/#{cluster}/status.json?network_address=#{node}.#{site}.grid5000.fr&disks=no&waiting=no"
    $logger.info("Fetching #{url}")
    d = open(url, {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}).read
  rescue
    tries -= 1
    if tries > 0
      $logger.info("Fetching #{url} failed. Sleeping 1s and retrying.")
      sleep(1)
      retry
    else
      $logger.info("Fetching #{url} failed too many times...")
      return nil
    end
  end
  begin
    j = JSON::parse(d)['nodes']["#{node}.#{site}.grid5000.fr"]
    if j['soft'] != 'busy'
      $logger.warn("Node is not busy according to API!")
      return nil
    else
      jr = j['reservations'].select { |r| r['state'] == 'running' && r['types'].include?('deploy') }
      $logger.info("Current reservation(s) on node: #{jr.map { |r| "#{r['uid']} (#{r['user']})" }.join(', ')}")
      return jr.map { |r| r['user'] }.uniq.first
    end
  rescue
    $logger.info("Parsing node information raised exception: #{$!}")
    return nil
  end
end
