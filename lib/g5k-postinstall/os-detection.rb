def linux_distrib
  extraction_dir = ENV['KADEPLOY_ENV_EXTRACTION_DIR']

  # It seems that the file os-release is a systemd thing but also present in some
  # Linux distributions whitout systemd…
  if File.exist?("#{extraction_dir}/etc/os-release")
    File.read("#{extraction_dir}/etc/os-release").each_line do |line|
      if line =~ /^ID="?([a-zA-Z0-9\-_]+)"?$/
        return $1
      end
    end
  else
    raise('unable to read /etc/os-release')
  end
end
