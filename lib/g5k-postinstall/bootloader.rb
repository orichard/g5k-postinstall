def install_grub2
  os_kind = ENV['KADEPLOY_OS_KIND']
  block_device = ENV['KADEPLOY_BLOCK_DEVICE']
  extraction_dir = ENV['KADEPLOY_ENV_EXTRACTION_DIR']
  deploy_part = ENV['KADEPLOY_DEPLOY_PART']
  deploy_part_num = ENV['KADEPLOY_DEPLOY_PART_NUM']
  kernel = ENV['KADEPLOY_ENV_KERNEL']
  kernel_params = ENV['KADEPLOY_ENV_KERNEL_PARAMS']
  initrd = ENV['KADEPLOY_ENV_INITRD']
  hypervisor_params = ENV['KADEPLOY_ENV_HYPERVISOR_PARAMS'] if os_kind == 'xen'
  cluster = ENV['KADEPLOY_CLUSTER']

  # Workaround for pyxis, grub detect the deployed disk as hd1
  if cluster == 'pyxis'
    deploy_disk_num = 1
  else
    deploy_disk_num = ('a'..'z').to_a.index(block_device[-1])
  end

  if os_kind == 'linux' && (linux_distrib == 'redhat' || linux_distrib == 'centos')
    grub_name = 'grub2'
  else
    grub_name = 'grub'
  end

  grub_dir = "#{extraction_dir}/boot/#{grub_name}"
  menufile = "#{grub_dir}/grub.cfg"

  unless File.exist?(block_device)
    raise("device not found #{block_device}")
  end

  FileUtils.mkdir_p(grub_dir)

  cpu_arch = `uname -m`.strip
  if cpu_arch == 'ppc64le'
    # No need to install anything, petitboot parses the grub.cfg directly
    grub_install_cmd = ""
  elsif File.directory?('/sys/firmware/efi')
    if cpu_arch == 'x86_64'
      grub_target = 'x86_64-efi'
    elsif cpu_arch == 'aarch64'
      grub_target = 'arm64-efi'
    else
      raise("Unable to install grub2 for efi on unknown CPU architecture: #{cpu_arch}")
    end

    efi = true
    efi_dir = '/boot/efi'
    FileUtils.mkdir_p("#{extraction_dir}#{efi_dir}")
    grub_install_cmd = "#{grub_name}-install --target=#{grub_target} --efi-directory=#{efi_dir}"
    grub_install_cmd += " --bootloader-id=part#{deploy_part_num}"
    grub_install_cmd += " --boot-directory=/boot"

    grub_mkimage_cmd = "#{grub_name}-mkimage -o #{efi_dir}/EFI/part#{deploy_part_num}/boot.efi"
    grub_mkimage_cmd += " --format=#{grub_target} \"--prefix=(hd#{deploy_disk_num},gpt#{deploy_part_num})/boot/#{grub_name}\""
    grub_mkimage_cmd += " ext2 part_gpt"
  else
    grub_install_cmd = "#{grub_name}-install --no-floppy --force #{deploy_part}"
  end

  grub_mkconfig_cmd = "#{grub_name}-mkconfig -o /boot/#{grub_name}/grub.cfg"
  grub_default = File.read("#{extraction_dir}/etc/default/grub")

  if os_kind == 'linux' || os_kind == 'xen'
    grub_default.gsub!(/GRUB_DEFAULT=.+/, "GRUB_DEFAULT=1")
    grub_default.gsub!(/GRUB_TIMEOUT=.+/, "GRUB_TIMEOUT=0")
    grub_default.gsub!(/GRUB_ENABLE_BLSCFG=.+/, "GRUB_ENABLE_BLSCFG=false")
    grub_default.gsub!(/GRUB_CMDLINE_LINUX=.+/, "GRUB_CMDLINE_LINUX=\"#{kernel_params}\"")

    File.open("#{extraction_dir}/etc/default/grub", 'w') do |f|
      f.write("#{grub_default}\n")
      f.write("GRUB_DISABLE_OS_PROBER=true\n")
      f.write("GRUB_DISABLE_LINUX_UUID=true\n")
      f.write("GRUB_CMDLINE_XEN=#{hypervisor_params}\n") if os_kind == 'xen'
      f.write("XEN_OVERRIDE_GRUB_DEFAULT=1\n") if os_kind == 'xen'
    end

    system_or_raise("mount --bind /dev #{extraction_dir}/dev")
    system_or_raise("mount --bind /sys #{extraction_dir}/sys")
    system_or_raise("mount -t proc none #{extraction_dir}/proc")
    system_or_raise("chroot #{extraction_dir} mount #{block_device}4 #{efi_dir}") if efi
    system_or_raise("chroot #{extraction_dir} #{grub_install_cmd}") if grub_install_cmd != ""
    system_or_raise("chroot #{extraction_dir} #{grub_mkimage_cmd}") if efi
    system_or_raise("chroot #{extraction_dir} #{grub_mkconfig_cmd}")
    if linux_distrib == 'debian' || linux_distrib == 'ubuntu'
      if system("echo 'get grub-pc/install_devices' | chroot #{extraction_dir} debconf-communicate")
        system_or_raise("echo 'set grub-pc/install_devices #{deploy_part}' | chroot #{extraction_dir} debconf-communicate")
      end
      if system("echo 'get grub-installer/choose_bootdev' | chroot #{extraction_dir} debconf-communicate")
        system_or_raise("echo 'set grub-installer/choose_bootdev #{deploy_part}' | chroot #{extraction_dir} debconf-communicate")
      end
    end
  else
    raise("OS not supported: #{os_kind}. Please contact the Grid'5000 team.")
  end
end
