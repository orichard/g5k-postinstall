# Mapping between IP ranges and sites (used to determine current site)

SITE_IP_RANGE = [
  ['grenoble', '172.16.16.0/20'],
  ['lille', '172.16.32.0/20'],
  ['lyon', '172.16.48.0/20'],
  ['nancy', '172.16.64.0/20'],
  ['rennes', '172.16.96.0/20'],
  ['sophia', '172.16.128.0/20'],
  ['luxembourg', '172.16.176.0/20'],
  ['nantes', '172.16.192.0/20']
]

# site-specific mountpoints

MOUNTPOINTS = {
  'lille' => {
    'dirs' => %w{/softs},
    'fstab' => <<-EOF
nfs:/export/softs /softs  nfs _netdev,rw,hard,async,nodev,nosuid,auto 0 0
    EOF
  },
  'grenoble' => {
    'dirs' => %w{/applis},
    'fstab' => <<-EOF
nfs:/export/applis  /applis nfs _netdev,rw,hard,async,nodev,nosuid,auto 0 0
    EOF
  }
}
