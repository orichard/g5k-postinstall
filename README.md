This repository contains a rewrite of the kadeploy postinstall for Grid'5000. This new version:

* aims at covering all use cases with a single script
* is written in Ruby instead of shell

Documentation: https://www.grid5000.fr/mediawiki/index.php/TechTeam:Postinstalls

# How to add a new cluster

1. Add site/cluster, node range and ip prefix on grid5000.yml

2. Create ssh keys

>       $ rake setup SITE=grenoble CLUSTER=yeti

3. Git add / commit changes

# Development information

The postinstall is executed like this by Kadeploy/taktuk in the deployment kernel:
```
KADEPLOY_CLUSTER="paranoia" \
KADEPLOY_DEPLOY_PART="/dev/sda3" \
KADEPLOY_BLOCK_DEVICE="/dev/sda" \
KADEPLOY_DEPLOY_PART_NUM="3" \
KADEPLOY_SWAP_PART_NUM="1" \
KADEPLOY_PROD_PART_NUM="2" \
KADEPLOY_TMP_PART_NUM="5" \
KADEPLOY_PREPOST_EXTRACTION_DIR="/rambin" \
KADEPLOY_TMP_DIR="/tmp" \
KADEPLOY_ENV="debian9-x64-min" \
KADEPLOY_ENV_KERNEL="/vmlinuz" \
KADEPLOY_ENV_INITRD="/initrd.img" \
KADEPLOY_ENV_KERNEL_PARAMS="console=tty0 console=ttyS0,38400n8 net.ifnames=0 biosdevname=0" \
KADEPLOY_ENV_HYPERVISOR="" \
KADEPLOY_ENV_HYPERVISOR_PARAMS="" \
KADEPLOY_OS_KIND="linux" \
KADEPLOY_PART_TYPE="83" \
KADEPLOY_FS_TYPE="ext4" \
KADEPLOY_ENV_EXTRACTION_DIR="/mnt/dest" \
/rambin/g5k-postinstall [options]
```

You can test the execution of g5k-postinstall from the prompt of the kadeploy
deployment kernel in kaconsole. Please mind setting the correct values, e.g.
the correct KADEPLOY_CLUSTER value.

See `g5k-postinstall --help` output for
the available options.

## Code structure

Most of the code is in g5k-postinstall itself. It's a script that can be read
top-to-bottom. To avoid cluttering this script, there are also a few utility
functions in lib/g5k-postinstall/.

## version file

The version file is mandatory and its content must match the version from
debian/changelog file
